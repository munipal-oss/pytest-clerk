## [4.0.3](https://gitlab.com/munipal-oss/pytest-clerk/compare/v4.0.2...v4.0.3) (2025-01-30)

### Bug Fixes

* reduce rate limit values by 1 across the board ([3da9dbf](https://gitlab.com/munipal-oss/pytest-clerk/commit/3da9dbf307b222c51455f7efec6eb4fab479b1e8))

## [4.0.2](https://gitlab.com/munipal-oss/pytest-clerk/compare/v4.0.1...v4.0.2) (2025-01-30)

### Bug Fixes

* respect clerk API rate limits ([f50ea74](https://gitlab.com/munipal-oss/pytest-clerk/commit/f50ea74b6a6f122db2aff86c28027494a034a427))

## [4.0.1](https://gitlab.com/munipal-oss/pytest-clerk/compare/v4.0.0...v4.0.1) (2024-11-29)

### Bug Fixes

* **deps:** update dependency httpx to ^0.28.0 ([8f0645c](https://gitlab.com/munipal-oss/pytest-clerk/commit/8f0645c55e8801054451e131f7d89ed5af8ba1c8))

## [4.0.0](https://gitlab.com/munipal-oss/pytest-clerk/compare/v3.0.0...v4.0.0) (2024-10-08)

### ⚠ BREAKING CHANGES

* **deps:** drop support for pytest-aws-fixtures ^2.0.0

### Bug Fixes

* **deps:** update dependency pytest-aws-fixtures to v3 ([deac53c](https://gitlab.com/munipal-oss/pytest-clerk/commit/deac53c0152f54a4b8e7d019f78148b1d4bae643))

## [3.0.0](https://gitlab.com/munipal-oss/pytest-clerk/compare/v2.1.0...v3.0.0) (2024-10-07)

### ⚠ BREAKING CHANGES

* **deps:** drop support for pre-commit ^3.7.0

### Miscellaneous Chores

* **deps:** update dependency pre-commit to v4 ([9cb17c3](https://gitlab.com/munipal-oss/pytest-clerk/commit/9cb17c36a9f38e0f3322202d603f6c85d721108b))

## [2.1.0](https://gitlab.com/munipal-oss/pytest-clerk/compare/v2.0.0...v2.1.0) (2024-08-15)

### Features

* add a new fixture clerk_end_user_session ([7833d0c](https://gitlab.com/munipal-oss/pytest-clerk/commit/7833d0c1fd4f284b39646957622a971a4f958cb8))

## [2.0.0](https://gitlab.com/munipal-oss/pytest-clerk/compare/v1.1.0...v2.0.0) (2024-07-30)

### ⚠ BREAKING CHANGES

* **deps:** drop support for tenacity ^8.2.3 and pytest-aws-fixtures ^1.1.0

### Bug Fixes

* **deps:** update dependency tenacity to v9 and pytest-aws-fixtures to v2 ([eb71c85](https://gitlab.com/munipal-oss/pytest-clerk/commit/eb71c85b96e62c623c77b237a347965f27e5f97c))

## [1.1.0](https://gitlab.com/munipal-oss/pytest-clerk/compare/v1.0.1...v1.1.0) (2024-06-27)

### Features

* add fixture to update a clerk org ([e468da9](https://gitlab.com/munipal-oss/pytest-clerk/commit/e468da9b8f702e16728d2779d6b5181fb107460a))

## [1.0.1](https://gitlab.com/munipal-oss/pytest-clerk/compare/v1.0.0...v1.0.1) (2024-04-19)


### Bug Fixes

* resolve some fixture issues from porting ([d51ba24](https://gitlab.com/munipal-oss/pytest-clerk/commit/d51ba248545e666dd266a9766d1f15ce3628b2f9))

## 1.0.0 (2024-04-19)


### Features

* initial release ([37ec9b4](https://gitlab.com/munipal-oss/pytest-clerk/commit/37ec9b4dafee743ecd5d109f7c36725f4d3b7af4))
